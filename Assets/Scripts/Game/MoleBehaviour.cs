﻿using UnityEngine;
using System.Collections;

public class MoleBehaviour : MonoBehaviour {

	private SingletonPlane plane;

	/*
	 * Jumping Attributes
	 */
	public float jumpStrenght;
	public float jumpSpeed;

	private bool prevClick;
	public bool click;

	private AnimationCurve jumpUpAnimation;
	private AnimationCurve jumpDownAnimation;

	// Use this for initialization
	void Start () {
		plane = SingletonPlane.Instance;

		/*
		 * Animation Curves initialization
		 */

		jumpUpAnimation = new AnimationCurve(
			new Keyframe(0.0f,0.0f),
			new Keyframe(0.8f,0.95f),
			new Keyframe(1f,1f));

		jumpDownAnimation = new AnimationCurve(
			new Keyframe(0.0f,0.0f),
			new Keyframe(0.8f,0.95f),
			new Keyframe(1f,1f));

		//jump on start
		jump ();

	}
	
	// Update is called once per frame
	void Update () {
		if (prevClick != click) {
			prevClick = click;
			jump();
		}
	}

	private void jump() {
		Vector3 currentPosition = this.transform.position;
		TweenPosition tp = TweenPosition.Begin (this.gameObject, jumpSpeed, currentPosition+Vector3.up*jumpStrenght);
		tp.eventReceiver = this.gameObject;
		tp.callWhenFinished = "jumpMaxHeight";
		tp.animationCurve = jumpUpAnimation;
	}

	/**
	 * When it's called will perform a Jump Down animation 
	 */
	private void jumpMaxHeight() {
		Vector3 currentPosition = this.transform.position;
		TweenPosition tp = TweenPosition.Begin (this.gameObject, jumpSpeed, currentPosition+Vector3.down*jumpStrenght);
		tp.eventReceiver = this.gameObject;
		Destroy (this.gameObject);
	}
	/*
	 *
	 * Returns true if the object is under the plane, false otherwise
	 */
	private bool isUnderThePlane(){
		return (this.gameObject.transform.position.y+(this.gameObject.transform.lossyScale.y) < plane.gameObject.transform.position.y);
	}
}

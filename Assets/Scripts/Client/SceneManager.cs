using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public delegate void AsyncOperationCallback( AsyncOperation async );

public class ScenesManager  {
	
	public enum Scene {
		none = -1,
		intro = 0,
		game = 2,
		outro = 3,
	}
	
	private Dictionary<Scene, string> sceneStrings=new Dictionary<Scene, string>(){
		{Scene.intro, "intro"},
		{Scene.game, "valerio"},
		{Scene.outro, "outer"},
	};

	private List<Scene> menuScene = new List<Scene>() {
		Scene.intro,
		Scene.game,
		Scene.outro,
	};

	private List<Scene> raceScene = new List<Scene>() {
		Scene.game,
	};
	
	private Scene currentScene = Scene.none;
	private Scene previousScene = Scene.none;

	private static ScenesManager instance = null;
	public static ScenesManager getInstance(){
		if (instance==null) {
			instance = new ScenesManager();
		}
		return instance;
	}
	
	public void changingSceneTo(Scene targetScene){		
		this.previousScene=this.currentScene;
		this.currentScene=targetScene;
		Application.LoadLevel(sceneStrings[targetScene]);
	}

	public AsyncOperation asyncLoader;
	public IEnumerator PrepareAdditiveAsync( string targetScene, AsyncOperationCallback callback = null ){
		if (Client.hasPro) {
			Application.backgroundLoadingPriority = ThreadPriority.Low;
			AsyncOperation asyncL=Application.LoadLevelAdditiveAsync(targetScene);
			//asyncLoader.allowSceneActivation = true;
			yield return asyncL;
			if (callback!=null) {
				callback( asyncLoader );
			}
			asyncLoader = null;
		}
		else {
			Application.LoadLevelAdditive(targetScene);
			if (callback!=null) {
				callback(null);
			}
		}
	}
	
	private bool preparingLevel = false;
	public void PrepareLoadAsync( Scene targetScene, bool changeSceneWhenDone = false ){
		if (!preparingLevel) {
			preparingLevel = changeSceneWhenDone ? false : true;
			D.L("Preparing load level async "+targetScene.ToString());
			Application.backgroundLoadingPriority = ThreadPriority.Low;
			asyncLoader = Application.LoadLevelAsync( this.sceneStrings[targetScene] );
			asyncLoader.allowSceneActivation = changeSceneWhenDone;
		}
	}
	
	public void ActivatePreparedLevel( Scene targetScene, AsyncOperationCallback callback = null ){
		if( asyncLoader != null ){
			preparingLevel = false;
			D.L("Activating prepared level "+targetScene.ToString());
			asyncLoader.allowSceneActivation = true;
			this.previousScene=this.currentScene;
			this.currentScene=targetScene;
			if(callback!=null) {
				callback( asyncLoader );
			}
		}
		asyncLoader = null;
	}

	public Scene getCurrentScene(){
		if (this.currentScene == Scene.none) {
			foreach(KeyValuePair<Scene, string> i in sceneStrings) {
				if (i.Value.Equals(Application.loadedLevelName)) {
					return i.Key;
				}
			}
			return Scene.none;
		}
		else {
			return this.currentScene;
		}
	}
	
	public bool isMenuScene() {
		return menuScene.Contains(getCurrentScene());
	}
	
	public bool isRaceScene() {
		return raceScene.Contains(getCurrentScene());
	}
	
	public void setCurrentScene(Scene s){
		this.currentScene=s;
	}
	
	public Scene getPreviousScene(){
		return this.previousScene;
	}
	
	public void setPreviousScene(Scene s){
		this.previousScene=s;
	}	
}
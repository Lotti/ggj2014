using UnityEngine;
using System;
using System.Collections;

public class KeyListener : InstantiatedSingleton<KeyListener> {
	void Update() {
		if (Input.GetKeyUp(KeyCode.Escape)) {
			Application.Quit();
		}
	}
}
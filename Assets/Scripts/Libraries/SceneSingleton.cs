using UnityEngine;

/// <summary>
/// Be aware this will not prevent a non singleton constructor
///   such as `T myT = new T();`
/// To prevent that, add `protected T () {}` to your singleton class.
/// 
/// As a note, this is made as MonoBehaviour because we need Coroutines.
/// </summary>
public class SceneSingleton<T> : MonoBehaviour where T : MonoBehaviour
{
	private static T _instance;
	
	public static T Instance
	{
		get
		{
			//Debug.Log("Check RaceManager Instance");
			if (_instance == null)
			{
				_instance = (T) FindObjectOfType(typeof(T));
				
				if ( FindObjectsOfType(typeof(T)).Length > 1 )
				{
					D.E("[Singleton] Something went really wrong " +
					               " - there should never be more than 1 singleton!" +
					               " Reopenning the scene might fix it.");
					return _instance;
				}
				
				if (_instance == null)
				{
					GameObject singleton = new GameObject();
					_instance = singleton.AddComponent<T>();
					singleton.name = "(SceneSingleton) "+ typeof(T).ToString();

					//D.L("[Singleton] An instance of " + typeof(T) + " is needed in the scene, so '" + singleton + "' was created with DontDestroyOnLoad.");
				} else {
					//D.L("[Singleton] Using instance already created: "+_instance.gameObject.name);
				}
			}
			
			return _instance;
		}
	}

	public static bool IsIntanced(){
		return (_instance != null);
	}

	protected virtual void OnDestroy () {
		_instance=null;
	}
}
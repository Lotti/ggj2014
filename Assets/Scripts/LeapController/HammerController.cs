using UnityEngine;
using System.Collections;

public class HammerController : SceneSingleton<HammerController> {

	public bool started = false;
	private UnityHand uh;

	void Awake(){
		uh = this.GetComponent<UnityHand>();
	}

	void Start (){

	}
	
	void Update (){
		CheckRaycastHit();
	}

	void OnCollisionEnter(Collision coll){

	}

	void OnTriggerEnter(Collider coll){
		if (coll.gameObject.GetComponent<SimpleMole>() != null) {
			HandleMolePrefabCollision(coll.gameObject);
		}
	}

	public void ToggleFettinaMartello(float angle){
		if(angle > 0) {
			uh.State = UnityHand.HandStateGame.MARTELLO;
			this.transform.FindChild("Fettina").gameObject.SetActive(false);
			this.transform.FindChild("Martello").gameObject.SetActive(true);
		}
		else {
			uh.State = UnityHand.HandStateGame.FETTINA;
			this.transform.FindChild("Fettina").gameObject.SetActive(true);
			this.transform.FindChild("Martello").gameObject.SetActive(false);
		}
	}

	public UnityHand.HandStateGame GetHammerState(){
		return uh.State;
	}

	public void CheckRaycastHit(){

		if(uh.State == UnityHand.HandStateGame.FETTINA){
			Ray r = new Ray(this.transform.position + Vector3.down*10,Vector3.down);
			Debug.DrawRay(r.origin,r.direction*100f,Color.yellow);
			RaycastHit rhit = new RaycastHit();
			int layerMask = 1 << 10;
			if(Physics.Raycast(r,out rhit,100f,layerMask)){
				//Debug.Log("Collider: "+rhit.collider.gameObject.name);
				rhit.collider.GetComponent<SimpleMole>().jump();
			}
		}
	}


	private void HandleMolePrefabCollision(GameObject obj){

		switch(this.uh.State){
			case UnityHand.HandStateGame.FETTINA:
				ScoreHandler.Instance.addScore(-10);
			break;
			case UnityHand.HandStateGame.MARTELLO:
				ScoreHandler.Instance.addScore(5);
			break;
		}
	}
}
﻿using UnityEngine;
using System.Collections;

public class TimeHandler : SceneSingleton<ScoreHandler> {

	public static bool started = false;
	
	public static float TimeLeft { get { return timeLeft; } }
	private static float timeLeft;
	public string TimeLabelText = "Time left: ";

	void Start() {
		timeLeft = 60f*2.5f;
		this.renderTime();
	}
	
	void Update() {
		if (started) {
			timeLeft-= Time.deltaTime;
			renderTime();
			if (timeLeft <= 0) {
				timeLeft = 0;
				PlaneSpawnEngine.Instance.EndGame();
			}
		}
	}

	void renderTime(){
		this.gameObject.GetComponent<UILabel> ().text = TimeLabelText + ((int)timeLeft).ToString();
	}
}

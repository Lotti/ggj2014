﻿using UnityEngine;
using System.Collections;

public class CameraCapture : SceneSingleton<CameraCapture> {

	private bool webcamExists = false;
	public Texture backupTexture;

	#if !UNITY_STANDALONE_OSX
	private WebCamTexture webcamTexture;
	#else
	private Texture webcamTexture;
	#endif
	private Texture currentFace;
	private bool webcam = true;

	public Texture frame { 
		get {
			if (webcamExists) {
				if (webcam) {
					return webcamTexture;
				}
				else {
					return currentFace;
				}
			}
			else {
				return backupTexture;
			}
		}
	}
	
	void Awake() {
		#if UNITY_STANDALONE_OSX
		webcamExists = false;
		#else
		WebCamDevice[] devices = WebCamTexture.devices;
		webcamTexture = new WebCamTexture();
		if (devices.Length > 0) {
			webcamExists = true;
			webcamTexture.deviceName = devices[0].name;
			webcamTexture.Play();
		}
		#endif
	}

	/*
	void OnGUI()
	{
		if (webcamTexture.isPlaying) {
			if (GUILayout.Button("Take Face")) {
				webcam = false;
				TakeFace();
			}
		}
		else {
			if (GUILayout.Button("Play")) {
				webcam = true;
				webcamTexture.Play();
			}
		}
	}
	*/

	void TakeFace() {
		#if !UNITY_STANDALONE_OSX
		if (webcamExists) {
			webcamTexture.Pause();
			currentFace = webcamTexture;
			webcamTexture.Stop();
		}
		#endif
	}
}

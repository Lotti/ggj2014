using UnityEngine;
using System;

public class Client
{
	public const string version = "1";
	public const string OBBversion = "0";
	public static bool debug = true;
	public static bool hasPro;
	
	private static Client instance;
	public static Client getInstance() {
		if (instance == null) instance = new Client();
		return instance;
	}

	//ConfigApp
	public const int worldNum = 3;
	public const int levelOnWorld = 10;

	static Client() {
		#if UNITY_EDITOR
		hasPro = UnityEditorInternal.InternalEditorUtility.HasPro();
		#else
		hasPro = true;
		#endif
	}

	private Client() {}
}

﻿using UnityEngine;
using System.Collections.Generic;

public class sharkPath : MonoBehaviour {

	private int counter = 1;
	public GameObject shark;
	public List<GameObject> points = new List<GameObject>();
	private GameObject intShark;
	
	void Start () {
		intShark = shark.transform.FindChild("shark").gameObject;
		shark.transform.position = points[0].transform.position;
		moveShark();
		sharkRaise();
	}

	void moveShark() {
		shark.transform.forward = -(points[counter%points.Count].transform.position - shark.transform.position).normalized;
		TweenPosition t = TweenPosition.Begin(shark, 10f, points[counter%points.Count].transform.position);
		t.eventReceiver = this.gameObject;
		t.callWhenFinished = "moveShark";
		counter++;
	}

	void sharkRaise() {
		Vector3 v = intShark.transform.localPosition;
		TweenPosition t = TweenPosition.Begin(intShark, 20f, new Vector3(v.x,-v.y,v.z));
		t.eventReceiver = this.gameObject;
		t.callWhenFinished = "sharkRaise";
		t.animationCurve = new AnimationCurve(
			new Keyframe(0f,0f),
			new Keyframe(0.25f,1f),
			new Keyframe(0.5f,0f),
			new Keyframe(0.75f,0f),
			new Keyframe(1f,1f));
	}
}
using UnityEngine;
using System;
using System.Collections;

public class SimpleMole : MonoBehaviour {

	private static string[] audioEffects = {"man", "child", "child2", "woman", "woman"};

	public static float timeCheck = 5f;
	public static bool autoJump = false;
	public static float minLaziness = 1f;
	public static float jumpSpeedMin = 2f;
	public static float jumpSpeedMax = 3f;

	public static int moleKilled = 0;

	private float passedTime;

	private bool hitten = false;
	private float jumpSpeed = 0f;
	private float laziness = 0f;
	private bool jumping = false;
	private int matrixX, matrixZ;
	private AudioSource audioSource;
	public const float startY = -50f;

	void Awake() {
		audioSource = GetComponent<AudioSource>();
		audioSource.clip = Resources.Load<AudioClip>("SoundFX/"+audioEffects[UnityEngine.Random.Range(0,audioEffects.Length)]);
	}

	void Start() {
		passedTime = Time.time;
		this.gameObject.renderer.materials[1].mainTexture = CameraCapture.Instance.frame;
		jumpSpeed = UnityEngine.Random.Range(jumpSpeedMin,jumpSpeedMax);
		laziness = UnityEngine.Random.Range(0f,1f);

		do {
			matrixX = UnityEngine.Random.Range(0,PlaneSpawnEngine.Instance.NumberX);
			matrixZ = UnityEngine.Random.Range(0,PlaneSpawnEngine.Instance.NumberZ);
		} while (PlaneSpawnEngine.Instance.Matrix[matrixX,matrixZ]);
		
		PlaneSpawnEngine.Instance.Matrix[matrixX,matrixZ] = true;
		int i = matrixX - PlaneSpawnEngine.Instance.NumberX/2;
		int j = matrixZ - PlaneSpawnEngine.Instance.NumberZ/2;
		float x = (i+1)*PlaneSpawnEngine.Instance.MaxX-(transform.localScale.x/2f);
		float z = (j+1)*PlaneSpawnEngine.Instance.MaxZ-(transform.localScale.z/2f);
		transform.localPosition = new Vector3(x,startY,z);
	}

	void LateUpdate() {
		if (autoJump && (Time.time - passedTime) > timeCheck) {
			passedTime = Time.time;
			jump();
		}
	}

	public const float jumpHeight = 70f;
	public void jump() {
		if (!jumping) {
			bool randomJump;
			if (autoJump) {
				randomJump = (UnityEngine.Random.Range(0f,1f) < minLaziness);
			}
			else {
				randomJump = (UnityEngine.Random.Range(0f,1f) >= laziness);
			}

			if (randomJump) {
				jumping = true;
				audioSource.Play();
				Vector3 p = transform.localPosition;
				TweenPosition t = TweenPosition.Begin(this.gameObject, jumpSpeed, new Vector3(p.x, p.y+jumpHeight, p.z));
				t.eventReceiver = this.gameObject;
				t.callWhenFinished = "duck";
				t.animationCurve = new AnimationCurve(
					new Keyframe(0f,0f),
					new Keyframe(0.9f,1f),
					new Keyframe(1f,1f));
				StartCoroutine(rotate());
			}
		}
	}
	
	void duck() {
		Vector3 p = transform.localPosition;
		TweenPosition t = TweenPosition.Begin(this.gameObject, jumpSpeed, new Vector3(p.x, p.y-jumpHeight, p.z));
		t.eventReceiver = this.gameObject;
		t.callWhenFinished = "ducked";
	}
	
	void ducked() {
		if (!hitten) {
			ScoreHandler.Instance.addScore(-3);
		}
		Destroy(this.gameObject);
		PlaneSpawnEngine.Instance.Matrix[matrixX,matrixZ] = false;
		instantiateMole();
	}

	private float spurt = 0.6f; //guizzo
	IEnumerator rotate() {
		yield return new WaitForSeconds(jumpSpeed*0.70f);

		if (!hitten) {
			Quaternion r = transform.rotation;
			Quaternion c = Quaternion.Euler(new Vector3(45f,0f,0f));

			TweenRotation t = TweenRotation.Begin(this.gameObject, spurt, r*c);
			t.eventReceiver = this.gameObject;
			t.callWhenFinished = "unrotate";
			t.animationCurve = new AnimationCurve(
				new Keyframe(0f,0f),
				new Keyframe(0.3f,1f),
				new Keyframe(0.6f,0f),
				new Keyframe(1f,1f));
		}
	}

	void unrotate() {
		Quaternion r = transform.rotation;
		Quaternion c = Quaternion.Euler(new Vector3(120f,0f,0f));

		TweenRotation t = TweenRotation.Begin(this.gameObject, spurt, r*c);
		t.animationCurve = new AnimationCurve(
			new Keyframe(0f,0f),
			new Keyframe(1f,1f));
	}

	void OnTriggerEnter(Collider c) {
		if (c.GetComponent<HammerController>() != null && HammerController.Instance.GetHammerState() == UnityHand.HandStateGame.MARTELLO) {
			this.hitten = true;
			moleKilled++;
			SoundManager.playFX(soundFX.batPunch);
			TweenRotation rot = GetComponent<TweenRotation>();
			if (rot != null) {
				rot.enabled = false;
			}
			this.transform.rotation = Quaternion.Euler(new Vector3(0f,0f,180f));
			foreach(Material m in renderer.materials) {
				m.color = Utils.darkenColor(m.color, 1.5f, 1f);
			}
			
			Vector3 p = this.transform.position;
			TweenPosition t = TweenPosition.Begin(this.gameObject, jumpSpeed, new Vector3(p.x,startY,p.z));
			t.eventReceiver = this.gameObject;
			t.callWhenFinished = "die";
		}
	}

	private float dieAnimation = 5f;
	void die() {
		Vector3 p = transform.position;
		this.transform.rotation = Quaternion.Euler(new Vector3(90f,90f,0f));
		TweenPosition t = TweenPosition.Begin(this.gameObject, dieAnimation, new Vector3(p.x,-3f,p.z));
		t.eventReceiver = this.gameObject;
		t.callWhenFinished = "ducked";
		t.animationCurve = new AnimationCurve(
			new Keyframe(0f,0f),
			new Keyframe(0.6f,1f),
			new Keyframe(0.7f,1.1f),
			new Keyframe(0.8f,1f),
			new Keyframe(0.9f,1.1f),
			new Keyframe(1f,1f));
	}

	private static int counter = 0;
	public static void instantiateMole() {
		counter++;
		GameObject t = (GameObject) Instantiate(PlaneSpawnEngine.Instance.molePrefab);
		t.name = "Mole "+counter;
		Color c = Utils.randomColor();
		t.renderer.materials[0].color = c;
		t.renderer.materials[2].color = c;
		t.transform.parent = PlaneSpawnEngine.Instance.moleContainer.transform;
		t.SetActive(true);
	}
}
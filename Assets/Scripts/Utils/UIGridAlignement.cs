﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Add this script like components on every grid element that you want to align to center.
/// For now it align gid elements to center x of screen and keep y immutate
/// </summary>

[RequireComponent(typeof(UIGrid))]
[RequireComponent(typeof(UISprite))]
public class UIGridAlignement : FinderBaseClass {

	public float size = 0;

	private UIGrid grid;
	private UIRect gridAnchor;

	void Awake(){
		grid = GetComponent<UIGrid>();
		gridAnchor = GetComponent<UISprite>();
		grid.onReposition = new UIGrid.OnReposition(updateAlignement);
	}

	// Use this for initialization
	void Start () {
		grid.Reposition();
	}
	
	public void updateAlignement(){
		Transform myT = transform;
		for (int i = 0; i < myT.childCount; i++){
			Transform ch = myT.GetChild(i);
			if (!NGUITools.GetActive(ch.gameObject) && grid.hideInactive) continue;
			size += grid.cellWidth;
			
		}
		float adjust = grid.cellWidth - myT.GetChild(myT.childCount-1).GetComponent<UISprite>().width;
		float tunes = myT.GetChild(myT.childCount-1).GetComponent<UISprite>().width/2f;
		size = size - adjust;
		NGUIMath.MoveWidget (gridAnchor,-1f * (size/2f) + tunes,0f);
	}
}

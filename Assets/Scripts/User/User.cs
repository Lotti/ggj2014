using UnityEngine;
using System;
using System.Collections.Generic;

public class User
{
	/*
	private static User _instance = null;
	public static User Instance {
		get { 
			if (_instance == null) {
				_instance = new User();
			} 
			return _instance;
		}
	}
	*/

	private User() {}

	static User() {
		restoreDefault();
	}
	private static void restoreDefault(){
	}

	public static void deleteLocalData() {
		PPCache.DeleteAll();
		restoreDefault();
	}
	
	private static string ppMusicEnabled = Utils.hash("music");
	private static bool isMusicEnabled() {
		return PPCache.GetInt(ppMusicEnabled,1) == 1 ? true : false;
	}
	private static void setMusicEnabled(bool enabled) {
		PPCache.SetInt(ppMusicEnabled, enabled ? 1 : 0);
	}
	
	private static string ppSFXEnabled = Utils.hash("sfx");
	private static bool isSFXEnabled() {
		return PPCache.GetInt(ppSFXEnabled,1) == 1 ? true : false;
	}
	private static void setSFXEnabled(bool enabled) {
		PPCache.SetInt(ppSFXEnabled, enabled ? 1 : 0);
	}

	public static bool isAudioEnabled() {
		return isMusicEnabled() && isSFXEnabled();
	}
	public static void setAudioEnabled(bool enabled) {
		setMusicEnabled(enabled);
		setSFXEnabled(enabled);
	}
}
﻿using UnityEngine;
using System.Collections;

public class PesceIntro : MonoBehaviour {
	
	void Start() {
		renderer.materials[1].mainTexture = CameraCapture.Instance.frame;
		transform.localScale = new Vector3(0f,0f,0f);
		TweenScale t = TweenScale.Begin(this.gameObject, 5f, new Vector3(10f,10f,10f));
		t.eventReceiver = this.gameObject;
		t.callWhenFinished = "playMenu";
		t.animationCurve = new AnimationCurve(
			new Keyframe(0f,0f),
			new Keyframe(0.8f,1f),
			new Keyframe(1f,1f));
	}

	void playMenu() {
		ScenesManager.getInstance().changingSceneTo(ScenesManager.Scene.game);
	}
}

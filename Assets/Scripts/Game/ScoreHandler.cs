﻿using UnityEngine;
using System.Collections;

public class ScoreHandler : SceneSingleton<ScoreHandler> {

	private static float multi;

	public static long Score { get { return score; } }
	private static long score;
	public string ScoreLabelText = "Score: ";
	// Use this for initialization
	void Start () {
		multi = 1f;
		score = 0;
		this.renderScore ();
	}
	
	// Update is called once per frame
	void Update() {
		if (ScoreHandler.Score > 50) {
			SimpleMole.autoJump = true;
		}

		float min = 0.5f;
		float max = 0.9f;
		float maxScore = 500f;
		if (ScoreHandler.Score <= maxScore) {
			SimpleMole.minLaziness = Mathf.Min(max, Mathf.Max(1-(maxScore-score)/maxScore, min));
		}
		else {
			SimpleMole.minLaziness = max;
		}

		if (ScoreHandler.Score > 500) {
			SimpleMole.jumpSpeedMin = 0.75f;
			SimpleMole.jumpSpeedMax = 1.75f;
			SimpleMole.timeCheck = 30f;
		}
		else if (ScoreHandler.Score > 300) {
			SimpleMole.jumpSpeedMin = 1f;
			SimpleMole.jumpSpeedMax = 2f;
			SimpleMole.timeCheck = 25f;
		}
		else if (ScoreHandler.Score > 200) {
			SimpleMole.jumpSpeedMin = 1.25f;
			SimpleMole.jumpSpeedMax = 2.25f;
			SimpleMole.timeCheck = 20f;
		}
		else if (ScoreHandler.Score > 100) {
			SimpleMole.jumpSpeedMin = 1.5f;
			SimpleMole.jumpSpeedMax = 2.5f;
			SimpleMole.timeCheck = 15f;
		}
		else if (ScoreHandler.Score > 50) {
			SimpleMole.jumpSpeedMin = 2f;
			SimpleMole.jumpSpeedMax = 3f;
			SimpleMole.timeCheck = 10f;
		}
	}

	public void addScore(long added) {
		if (added > 0) {
			multi+= 0.1f;
		}
		else {
			multi = 1f;
		}

		score += (long) (added*multi);
		renderScore();
	}
	
	public void resetScore(){
		score = 0;
	}

	void renderScore(){
		this.gameObject.GetComponent<UILabel> ().text = ScoreLabelText + score.ToString();
	}


}

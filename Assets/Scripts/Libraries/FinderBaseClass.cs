using UnityEngine;
using System.Collections.Generic;

public class FinderBaseClass : MonoBehaviour
{
	private Dictionary<string, UIAnchor> anchor = new Dictionary<string, UIAnchor>();
	//private Dictionary<string, UILinearLayout> layout = new Dictionary<string, UILinearLayout>();
	private Dictionary<string, UILabel> labels = new Dictionary<string, UILabel>();
	private Dictionary<string, UISprite> sprites = new Dictionary<string, UISprite>();
	//private Dictionary<string, UIDoubleSlider> doubleSliders = new Dictionary<string, UIDoubleSlider>();
	private Dictionary<string, UISlider> sliders = new Dictionary<string, UISlider>();
	//private Dictionary<string, UIRoundedLoader> roundedLoaders = new Dictionary<string, UIRoundedLoader>();
	private Dictionary<string, UITexture> textures = new Dictionary<string, UITexture>();
	private Dictionary<string, UIPanel> panels = new Dictionary<string, UIPanel>();	
	private Dictionary<string, GameObject> gameobjects = new Dictionary<string, GameObject>();
	
	public void resetHash(){
		this.labels.Clear();
		//this.layout.Clear();
		this.sprites.Clear();
		//this.doubleSliders.Clear();
		this.sliders.Clear();
		//this.roundedLoaders.Clear();
		this.gameobjects.Clear();
		this.textures.Clear();
		this.panels.Clear();
	}
	
	public UISprite getSprite(string spriteName){
		UISprite s=null;
		if(this.sprites.ContainsKey(spriteName))
			s=(UISprite) this.sprites[spriteName];
		else{
			GameObject go=this.gameObject.transform.FindChild(spriteName).gameObject;
			if(go==null){
				D.L("Null Reference: "+spriteName);
			}
			else {
				s=go.GetComponent<UISprite>();
				this.sprites.Add(spriteName, s);
			}
		}
		return s;
	}
	
	public GameObject getGO(string labelName){	
		GameObject go=null;
		if(this.gameobjects.ContainsKey(labelName))
			go=(GameObject) this.gameobjects[labelName];
		else{
			Transform t=this.gameObject.transform.FindChild(labelName);
			if(t==null) {
				D.L("Null Reference: "+labelName);
			}
			else{
				go=t.gameObject;
				if(go!=null){
					this.gameobjects.Add(labelName,go);
				}
			}
		}
		return go;
	}
	
	public UILabel getLabel(string labelName){	
		UILabel l=null;
		if(this.labels.ContainsKey(labelName))
			l=(UILabel) this.labels[labelName];
		else{
			Transform t=this.gameObject.transform.FindChild(labelName);
			if(t==null) {
				D.L("Null Reference: "+labelName);
			}
			else{
				GameObject go=t.gameObject;
				if(go!=null){
					l=go.GetComponent<UILabel>();
					this.labels.Add(labelName, l);
				}
			}
		}
		return l;
	}
	
	public UIAnchor getAnchor(string anchorName){	
		UIAnchor a=null;
		if(this.anchor.ContainsKey(anchorName))
			a=(UIAnchor) this.anchor[anchorName];
		else{
			Transform t=this.gameObject.transform.FindChild(anchorName);
			if(t==null) {
				D.L("Null Reference: "+anchorName);
			}
			else {
				GameObject go=t.gameObject;
				if(go!=null){
					a=go.GetComponent<UIAnchor>();
					this.anchor.Add(anchorName, a);
				}
			}
		}
		return a;
	}

	/*
	public UILinearLayout getLinearLayout(string linearLayoutName){	
		UILinearLayout l=null;
		if(this.layout.ContainsKey(linearLayoutName))
			l=(UILinearLayout) this.layout[linearLayoutName];
		else{
			Transform t=this.gameObject.transform.FindChild(linearLayoutName);
			if(t==null) {
				D.L("Null Reference: "+linearLayoutName);
			}
			else{
				GameObject go=t.gameObject;
				if(go!=null){
					l=go.GetComponent<UILinearLayout>();
					this.layout.Add(linearLayoutName, l);
				}
			}
		}
		return l;
	}
	*/

	/*
	public UIDoubleSlider getDoubleSlider(string doubleSliderName){	
		UIDoubleSlider s=null;
		if(this.doubleSliders.ContainsKey(doubleSliderName))
			s=(UIDoubleSlider) this.doubleSliders[doubleSliderName];
		else{
			GameObject go=this.gameObject.transform.FindChild(doubleSliderName).gameObject;
			if(go==null){
				D.L("Null Reference: "+doubleSliderName);
			}
			else {
				s=go.GetComponent<UIDoubleSlider>();
				this.doubleSliders.Add(doubleSliderName, s);
			}
		}
		return s;
	}
	*/
	
	public UISlider getSlider(string sliderName){	
		UISlider s=null;
		if(this.sliders.ContainsKey(sliderName))
			s=(UISlider) this.sliders[sliderName];
		else{
			GameObject go=this.gameObject.transform.FindChild(sliderName).gameObject;
			if(go==null){
				D.L("Null Reference: "+sliderName);
			}
			else {
				s=go.GetComponent<UISlider>();
				this.sliders.Add(sliderName, s);
			}
		}
		return s;
	}	

	/*
	public UIRoundedLoader getRoundedLoader(string roundedLoaderName){	
		UIRoundedLoader l=null;
		if(this.roundedLoaders.ContainsKey(roundedLoaderName))
			l=(UIRoundedLoader) this.roundedLoaders[roundedLoaderName];
		else{
			Transform t=this.gameObject.transform.FindChild(roundedLoaderName);
			if(t==null) {
				D.L("Null Reference: "+roundedLoaderName);
			}
			else{
				GameObject go=t.gameObject;
				if(go!=null){
					l=go.GetComponent<UIRoundedLoader>();
					this.roundedLoaders.Add(roundedLoaderName, l);
				}
			}
		}
		return l;
	}
	*/
	
	public UITexture getTexture(string name){	
		UITexture s=null;
		if(this.textures.ContainsKey(name))
			s=(UITexture) this.textures[name];
		else{
			Transform t=this.gameObject.transform.FindChild(name);
			if( t!=null ){
				GameObject go=t.gameObject;
				if(go==null){
					D.L("Null Reference: "+name);
				}
				else {
					s=go.GetComponent<UITexture>();
					this.textures.Add(name, s);
				}
			}
		}
		return s;
	}	

	public UIPanel getPanel(string name){	
		UIPanel s=null;
		if(this.panels.ContainsKey(name))
			s=(UIPanel) this.panels[name];
		else{
			GameObject go=this.gameObject.transform.FindChild(name).gameObject;
			if(go==null){
				D.L("Null Reference: "+name);
			}
			else {
				s=go.GetComponent<UIPanel>();
				this.panels.Add(name, s);
			}
		}
		return s;
	}	

}
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class PPCache {
	private PPCache() {}
	
	private static Hashtable register = null;

	private static void addValue(object key, object value) {
		if (register == null) {
			register = new Hashtable();
		}
		
		if (register.ContainsKey(key)) {
			register[key] = value;
		}
		else {
			register.Add(key, value);
		}
	}

	public static void SetInt(string key, int value) {
		try {
			PlayerPrefs.SetInt(key,value);
			Save();
			addValue(key,value);
		}
		catch(PlayerPrefsException) {
			throw;
		}
	}
	
	public static void SetFloat(string key, float value) {
		try {
			PlayerPrefs.SetFloat(key,value);
			Save();
			addValue(key,value);
		}
		catch(PlayerPrefsException) {
			throw;
		}
	}
	
	public static void SetString(string key, string value) {
		try {
			PlayerPrefs.SetString(key,value);
			Save();
			addValue(key,value);
		}
		catch(PlayerPrefsException) {
			throw;
		}
	}

	public static float GetFloat(string key, float defValue = 0.0f) {
		if (register == null) {
			register = new Hashtable();
		}
		if (!register.ContainsKey(key)) {
			if (PlayerPrefs.HasKey(key)) {
				addValue(key, PlayerPrefs.GetFloat(key));
			}
			else {
				return defValue;
			}
		}
		return (float)register[key];

	}

	public static int GetInt(string key, int defValue = 0) {
		if (register == null) {
			register = new Hashtable();
		}
		if (!register.ContainsKey(key)) {
			if (PlayerPrefs.HasKey(key)) {
				addValue(key, PlayerPrefs.GetInt(key));
			}
			else {
				return defValue;
			}
		}
		return (int)register[key];

	}

	public static string GetString(string key, string defValue = "") {
		if (register == null) {
			register = new Hashtable();
		}
		if (!register.ContainsKey(key)) {
			if (PlayerPrefs.HasKey(key)) {
				addValue(key, PlayerPrefs.GetString(key));
			}
			else {
				return defValue;
			}
		}
		return (string)register[key];

	}
	
	public static void DeleteAll() {
		PlayerPrefs.DeleteAll();
		Save();
		if (register != null) register.Clear();
	}
	
	public static void DeleteKey(string key) {
		PlayerPrefs.DeleteKey(key);
		Save();
		if (register != null) register.Remove(key);
	}

	public static bool HasKey(string key) {
		return PlayerPrefs.HasKey(key);
	}

	private static void Save() {
		PlayerPrefs.Save();
	}
}



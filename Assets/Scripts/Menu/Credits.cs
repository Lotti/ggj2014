﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Credits : FinderBaseClass {

	private List<UIPanel> panels = new List<UIPanel>();
	private int counter = 0;
	private MovieTexture movie;
	private bool movieLoaded = false;

	void Awake() {
		movie = Resources.Load<MovieTexture>("Video/video");
		panels.Add(this.getPanel("Credits1"));
		panels.Add(this.getPanel("Credits2"));

		foreach(UIPanel g in panels) {
			g.alpha = 0f;
		}
	}

	// Use this for initialization
	void Start () {
		creditsAnimation();
	}

	void creditsAnimation() {
		if (counter-1 >= 0) {
			panels[counter-1].gameObject.SetActive(false);
		}

		if (counter < panels.Count) {
			TweenAlpha t = TweenAlpha.Begin(panels[counter].gameObject, 5f, 1f);
			t.eventReceiver = this.gameObject;
			t.callWhenFinished = "creditsAnimation";
		}
		else {
			GetComponent<UITexture>().mainTexture = movie;
			GetComponent<AudioSource>().clip = movie.audioClip;
			movie.Play();
			GetComponent<AudioSource>().Play();
			movieLoaded = true;
		}
		counter++;
	}
	
	// Update is called once per frame
	void Update() {
		if (movieLoaded && !movie.isPlaying) {
			ScenesManager.getInstance().changingSceneTo(ScenesManager.Scene.game);
		}
	}

	void OnClick() {
		ScenesManager.getInstance().changingSceneTo(ScenesManager.Scene.game);
	}
}

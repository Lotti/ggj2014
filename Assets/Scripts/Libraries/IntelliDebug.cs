﻿using UnityEngine;
using System.Collections;

public static class D {

	public static string tag = "AK74";
	public static bool print;

	static D() {
		print = Client.debug;
	}

	public static void L(object m, LogType t = LogType.Log){
		if(!print){
			return;
		}
		switch(t){
			default:
			case LogType.Log:
				Debug.Log("[DEBUG]["+tag+"] " + m);
			break;
			case LogType.Assert:
				Debug.Log("[ASSERT]["+tag+"] " + m);
			break;
			case LogType.Warning:
				Debug.LogWarning("[WARNING]["+tag+"] " + m);
			break;
			case LogType.Error:
				Debug.LogError("[ERROR]["+tag+"] " + m);
			break;
		}
	}

	public static void L(System.Exception e){
		if(!print){
			return;
		}
		Debug.LogError("[EXCEPTION]["+tag+"]" + e.Message);
	}

	public static void W(object m) {
		L(m, LogType.Warning);
	}

	public static void E(object m) {
		L(m, LogType.Error);
	}

	public static void A(object m) {
		L(m, LogType.Assert);
	}
}



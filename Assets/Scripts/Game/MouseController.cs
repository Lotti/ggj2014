﻿using UnityEngine;
using System.Collections;

public class MouseController : MonoBehaviour {
	public float rotationDuration;
	public float rotationSpeed;

	private float rotationCounter;
	private float rotationDirection = 0;
	
	// Update is called once per frame
	void Update () {
		if(!LeapHandController.mouseControl){
			this.enabled = false;
		}
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		// Casts the ray and get the first game object hit
		int layerStack = 1 << 11;
		if (Physics.Raycast (ray, out hit, 1000,layerStack) ) {
			this.gameObject.transform.parent.transform.FindChild("MartelloFettina").transform.position=  new Vector3(hit.point.x,this.gameObject.transform.position.y,hit.point.z);
		}

		if (HammerController.Instance.started) {
			if (Input.GetMouseButtonDown(0) && rotationDirection == 0) {
				rotationDirection = 1;
			}
		}
		rotate();
	}

	private void rotate() {
		rotationCounter+=rotationDirection;

		this.gameObject.transform.parent.transform.FindChild("MartelloFettina").transform.Rotate (Vector3.right *rotationDirection*rotationSpeed);
			HammerController.Instance.ToggleFettinaMartello (Mathf.Abs(rotationDirection)*rotationSpeed);
		if (rotationCounter == rotationDuration)
			rotationDirection = -1;
		else if (rotationCounter == 0)
			rotationDirection = 0;
	}
}

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class PlaneSpawnEngine : SceneSingleton<PlaneSpawnEngine> {

	public UILabel totalScore;
	public int spawnQuantity = 5;
	public GameObject moleContainer;
	public GameObject molePrefab;
	public GameObject splashPrefab;
	public UITexture background;
	private const float scale = 10f;

	private bool[,] matrix;
	public bool[,] Matrix { get { return matrix; } }

	private int numberX;
	public int NumberX { get { return numberX; } }
	private int numberZ;
	public int NumberZ { get { return numberZ; } }

	private float maxX;
	public float MaxX { get { return maxX; } }
	private float maxZ;
	public float MaxZ { get { return maxZ; } }

	void Awake() {
		totalScore.gameObject.SetActive(false);
		molePrefab.SetActive(false);
		splashPrefab.SetActive(false);

		numberX = (int)(scale*transform.localScale.x / molePrefab.transform.localScale.x);
		numberZ = (int)(scale*transform.localScale.z / molePrefab.transform.localScale.z);
		maxX = transform.localScale.x/2f;
		maxZ = transform.localScale.z/2f;

		matrix = new bool[numberX,numberZ];
		for(int i = 0; i<numberX; i++) {
			for(int j = 0; j<numberZ; j++) {
				matrix[i,j] = false;
			}
		}
	}

	public void StartGame() {
		HammerController.Instance.started = true;
		TimeHandler.started = true;
		SimpleMole.moleKilled = 0;

		SoundManager.Instance.stopMusic();
		SoundManager.Instance.getMusicPlayer().clip = Resources.Load<AudioClip>("SoundFX/ocean");
		SoundManager.Instance.playMusic();

		background.gameObject.SetActive(false);
		for(int i = 0; i<spawnQuantity; i++) {
			SimpleMole.instantiateMole();
		}
	}

	public void EndGame() {
		foreach(SimpleMole s in moleContainer.transform.GetComponentsInChildren<SimpleMole>(true)) {
			Destroy(s.gameObject);
		}
		totalScore.gameObject.SetActive(true);
		totalScore.text = "Your final score is "+ScoreHandler.Score+"\n Fish killed: "+SimpleMole.moleKilled;
		StartCoroutine(goToCredits());
	}

	IEnumerator goToCredits() {
		yield return new WaitForSeconds(5f);
		ScenesManager.getInstance().changingSceneTo(ScenesManager.Scene.outro);
	}

	void OnTriggerEnter(Collider collider) {
		if (collider.gameObject.layer == 10) {
			GameObject splash = (GameObject)Instantiate(splashPrefab.gameObject);
			splash.transform.parent = transform;
			splash.transform.position = new Vector3(collider.transform.position.x, transform.position.y, collider.transform.position.z);
			splash.SetActive(true);
			StartCoroutine(destroyMe(splash, splash.GetComponent<ParticleSystem>().duration));
		}
	}

	IEnumerator destroyMe(GameObject g, float delay) {
		yield return new WaitForSeconds(delay);
		Destroy(g);
	}
}
﻿using UnityEngine;
using System;
using System.Collections;
using System.Globalization;

public class Utils {
	public static void DestroyImmediateAllChildren( Transform parent ) {
		for (int i = parent.childCount - 1; i >= 0; i--) {
			Transform objectA = parent.GetChild(i);
			objectA.parent = null;
#if UNITY_EDITOR
			GameObject.DestroyImmediate(objectA.gameObject);
#else
			GameObject.Destroy(objectA.gameObject);
#endif
		} 
	}

	public static Color fullColor = new Color(1f,1f,1f);
	public const char Euro = (char)0x20AC;
	
	private static CultureInfo cultureInfo = null;
	public static void setCultureInfo(CultureInfo c) {
		cultureInfo = c;
	}

	public static int GetWeekNumber(DateTime dtPassed) {
		int weekNum = cultureInfo.Calendar.GetWeekOfYear(dtPassed, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
		return weekNum;
	}

	public static Color randomColor() {
		return new Color(UnityEngine.Random.Range(0f,1f), UnityEngine.Random.Range(0f,1f), UnityEngine.Random.Range(0f,1f));
	}

	public static Color HexToColor(int pColor, float alpha = 1.0f) {
		Color color;
		color.r = ((pColor & 0xFF0000) >> 16) / 255.0f;
		color.g = ((pColor & 0x00FF00) >> 8) / 255.0f;
		color.b = (pColor & 0x0000FF) / 255.0f;
		color.a = alpha;
		
		return color;
	}
	
	public static Color HexToColor(string s) {
		return HexToColor(int.Parse(s, System.Globalization.NumberStyles.AllowHexSpecifier));
	}
	
	public static string ColorToHex(Color32 color) {
		string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
		return hex;
	}

	public static Color darkenColor(Color c, float factor, float alpha = -1f) {
		return new Color(c.r/factor, c.g/factor, c.b/factor, (alpha == -1) ? c.a : alpha);
	}

	public static Color brightenColor(Color c, float factor, float alpha = -1f) {
		return new Color(c.r*factor, c.g*factor, c.b*factor, (alpha == -1) ? c.a : alpha);
	}

	/*
	public static int WrapIdAround(int entryNumber, int difference, int arrayLenght)
	{
		int newId = (entryNumber + difference)%arrayLenght;
		if(newId < 0)
		{ newId += arrayLenght; }
		return newId;
	}
	*/
	
	public static string UCFirst(string s) {
		if (string.IsNullOrEmpty(s)) {
			return string.Empty;
		}
		return char.ToUpper(s[0]) + s.Substring(1);
	}

	/*
	public static string formatMinSec(int v){
		if(v<10) {
			return "0"+v;
		}
		else {
			return ""+v;
		}
	}
	*/

	/*
	public static string formatMillis(int millis){
		if(millis==0) {
			return "000";
		}
		else if(millis<10) {
			return "00"+millis;
		}
		else if(millis<100) {
			return "0"+millis;
		}
		else {
			return ""+millis;
		}
	}
	*/

	/*
	public static string formatCountDown(float time){
		int min=(int) time/60;
		int sec= (int) time - min*60;
		//int millis= (int) ((time - min*60 - sec)*1000);
		return ((min!=0)?(Utils.pad2(min) + ":"):"00:") + Utils.pad2(sec) + "";
	}

	public static string formatCountDownSeconds(float time){
		int sec= (int) time;
		int millis= (int) ((time - sec)*10);
		return ((sec!=0)?(Utils.pad2(sec) + ":"):"00:") + Utils.pad2(millis) + "";
	}
	*/

	/*
	public static string formatTimeMillis(float time){
		int min = (int) time/60;
		int sec = (int) time - min*60;
		int millis = (int) ((time - min*60 - sec)*1000);
		
		return ((min != 0) ? (min + ":"):"") + Utils.pad2(sec) + ":" + formatMillis(millis);
	}
	*/

	/*
	public static string formatTimeSeconds(int time) {
		return formatTimeSeconds((float) time);
	}
	
	public static string formatTimeSeconds(float time) {
		List<string> r = new List<string>();
		
		int day = Mathf.FloorToInt(time/60f/60f/24f);
		if (day > 0) {
			r.Add(day+"d");
			time-=day*60f*60f*24f;
		}
		int hour = Mathf.FloorToInt(time/60f/60f);
		if (hour > 0) {
			r.Add(hour+"h");
			time-=hour*60f*60f;
		}
		int min = Mathf.FloorToInt(time/60f);
		if (min > 0) {
			r.Add(min+"m");
			time-=min*60f;
		}
		int sec = Mathf.FloorToInt(time);
		if (sec >= 0) {
			r.Add(sec+"s");
		}
		
		if (r.Count >= 2) {
			return string.Join(" ", r.GetRange(0,2).ToArray());	
		}
		else {
			return r.ToArray()[0];
		}
	}
	*/
	
	public static string formatCurrency(int c, int d){
		return formatCurrency(c, d);
	}
	
	public static string formatCurrency(double c, int decimalLength = 0) {					
		if (decimalLength > 0) {
			c = double.Parse(c.ToString("F"+decimalLength));
		}
		
		int integer = (int)c;
		if (decimalLength > 0 || c-integer > 0) {
			return String.Format(cultureInfo, "{0:N}", c);
		}
		else {
			return String.Format(cultureInfo, "{0:N0}", integer);
		}
	}

	/*
	public static string formatK(float value) {
		return (value > 10000)?(value/10000f)+"k" : value.ToString();
	}
	*/

	/*
	public static double DateTimeToUnixTimestamp(DateTime dateTime) {
		return Math.Round((dateTime - new DateTime(1970, 1, 1).ToLocalTime()).TotalSeconds);
	}

	public static double getTimestamp() {
		return double.Parse(DateTimeToUnixTimestamp(DateTime.Now).ToString());
	}

	public static long getTimestamp() {
		return long.Parse(DateTimeToUnixTimestamp(DateTime.Now).ToString());
	}
	*/

	/*
	public static float deltaPerc(float val1, float val2, bool round = true) {
		float v = (val2 - val1) / val1 * 100;
		if (round) {
			v = Mathf.Round(v * 10) / 10;
		}
		return v;
	}
	*/

	/*
	public static string getPercDiff(float val1, float val2) {
		string v = "";
		if (val2-val1 != 0) {
			if (val2-val1 > 0) {
				v+= "+";
			}
			v+= (100*(val2-val1)/val1).ToString("F1")+"%";
		}
		return v;
	}
	*/

	/*
	public static void arrayShuffle<T>(T[] array) {
		// Knuth shuffle algorithm :: courtesy of Wikipedia :)
		for (int t = 0; t < array.Length; t++) {
			T tmp = array[t];
			int r = UnityEngine.Random.Range(t, array.Length);
			array[t] = array[r];
			array[r] = tmp;
		}
	}
	*/

	/*
	private static int uniqueId=0;
	public static string getUniqueId(){
		uniqueId++;
		return uniqueId.ToString();
	}
	*/

	/*
	public static string md5(string input)
	{
		#if UNITY_WP8
		byte[] data = MD5Core.GetHash(input);
		#else
		// Create a new instance of the MD5CryptoServiceProvider object.
		MD5 md5Hasher = MD5.Create();
		
		// Convert the input string to a byte array and compute the hash.
		byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));
		#endif
		
		// Create a new Stringbuilder to collect the bytes
		// and create a string.
		StringBuilder sBuilder = new StringBuilder();
		
		// Loop through each byte of the hashed data 
		// and format each one as a hexadecimal string.
		for (int i = 0; i < data.Length; i++) {
			sBuilder.Append(data[i].ToString("x2"));
		}
		
		// Return the hexadecimal string.
		return sBuilder.ToString();
	}
	*/
	
	public static string hash(string input) {
		return input.GetHashCode().ToString();
	}

	public static string hexEncrypt(int value, int constant) {
		return (value*constant).ToString("X");
	}

	public static string hexDecrypt(string hexValue, int constant) {
		return (int.Parse(hexValue, System.Globalization.NumberStyles.HexNumber) / constant).ToString();
	}
}

using UnityEngine;
using System.Collections;
 
public class FPSCounter : MonoBehaviour {
	public float frequency = 0.5f;
	public UILabel label;
	public int FramesPerSec { get; protected set; }
 
	void Awake() {
		if(label == null) {
			label = this.gameObject.GetComponent<UILabel>();
		}
	}
	
	void Start() {
		if (Client.debug) {
			label.gameObject.SetActive(true);
			StartCoroutine(FPS());
		}
		else {
			label.gameObject.SetActive(false);
		}
	}
 
	private IEnumerator FPS() {
		while (true) {
			// Capture frame-per-second
			int lastFrameCount = Time.frameCount;
			float lastTime = Time.realtimeSinceStartup;
			yield return new WaitForSeconds(frequency);
			float timeSpan = Time.realtimeSinceStartup - lastTime;
			int frameCount = Time.frameCount - lastFrameCount;
 
			// Display it
			FramesPerSec = Mathf.RoundToInt(frameCount / timeSpan);
			label.text = FramesPerSec.ToString() + " fps";
		}
	}
}
﻿using UnityEngine;
using System.Collections;

public class intro : MonoBehaviour {

	public UILabel[] introStrings;
	int c;
	const int playSoundIdx = 2;
	AudioSource	audioSource;
	public PesceIntro pesceIntro;

	void allOff(){
				foreach (UILabel l in introStrings)
						l.gameObject.SetActive (false);
	}

	void Awake(){
		Application.runInBackground = true;

		allOff ();

		audioSource = GetComponent<AudioSource>();
		//audioSource.clip = Resources.Load<AudioClip>("SoundFX/fettinepanate.wav");

		c = 0;
	}

	void fadeout(){
		TweenAlpha a = TweenAlpha.Begin (introStrings [c].gameObject, 2f, 0f);
		a.eventReceiver = this.gameObject;

		if (c < introStrings.Length - 1) {
				
			if (c == playSoundIdx) {
				audioSource.Play();
			}

		/*SoundManager.Instance.playMusic();
				Debug.Log("Played");
			}*/

			a.callWhenFinished = "animazione";
		} else {
				pesceIntro.gameObject.SetActive(true);
			}
		c++;
	}

	void animazione(){
		allOff ();

		introStrings [c].gameObject.SetActive (true);
		introStrings [c].alpha = 0f;

		TweenAlpha a = TweenAlpha.Begin (introStrings [c].gameObject, 3f,1f);
		a.eventReceiver = this.gameObject;

		a.callWhenFinished = "fadeout";
	}

	// Use this for initialization
	void Start () {
		SoundManager.Instance.playMusic();
		animazione ();
	}
	
	// Update is called once per frame
	void OnClick () {
		ScenesManager.getInstance().changingSceneTo(ScenesManager.Scene.game);
	}
}

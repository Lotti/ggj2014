using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundManager : PrefabSingleton<SoundManager> {
	public const float MIN_MUSIC_VOLUME = 0f;
	public const float MIN_SFX_VOLUME = 0f;	
	public const float MAX_MUSIC_VOLUME = 0.75f;
	public const float MAX_SFX_VOLUME = 1f;
	
	private AudioSource MusicPlayer = null;
	private AudioSource SFXPlayer = null;
	
	private bool musicEnabled = false;
	public bool MusicEnabled { get { return musicEnabled; } set { musicEnabled = value; } }
	private bool sfxEnabled = false;
	public bool SFXEnabled { get { return sfxEnabled; } set { sfxEnabled = value; } }
	
	private float musicVolume = 0f;
	public float MusicVolume { get { return musicVolume; } 
		set { 
			bool play = true;
			musicVolume = MAX_MUSIC_VOLUME;
			if (value <= MIN_MUSIC_VOLUME) {
				musicVolume = MIN_MUSIC_VOLUME;
				play = false;
			}			
			if (value >= MAX_MUSIC_VOLUME) {
				musicVolume = MAX_MUSIC_VOLUME;
			}
			
			MusicPlayer.volume = musicVolume;
			if (play) {
				playMusic();
				MusicEnabled = true;
			}
			else {
				stopMusic();
				MusicEnabled = false;
			}
		} }
	private float sfxVolume = 0f;
	public float SFXVolume { get { return sfxVolume; } 
		set { 
			sfxVolume = MAX_SFX_VOLUME;
			if (value <= MIN_SFX_VOLUME) {
				sfxVolume = MIN_SFX_VOLUME;
				SFXEnabled = false;
			}
			if (value >= MAX_SFX_VOLUME) {
				sfxVolume = MAX_SFX_VOLUME;
				SFXEnabled = true;
			}
			
			SFXPlayer.volume = sfxVolume;
		} }
 
	protected override void Awake() {
		base.Awake();

		MusicPlayer = this.gameObject.transform.FindChild("MusicPlayer").GetComponent<AudioSource>();
		SFXPlayer = this.gameObject.transform.FindChild("SFXPlayer").GetComponent<AudioSource>();		
	}
	
	void Start(){
		if (checkScene()) {
			if (Camera.main!=null) {
				transform.position=Camera.main.transform.position;
			}
		}			
	}
	
	void OnLevelWasLoaded(int level) {
		checkScene();
	}
	
	public bool checkScene() {
		/*
		if (MenuScenesManager.getInstance().isRaceScene()) {
			stopMusic();
			return false;
		} 
		else {
		*/
			//playMusic();
			return true;
		//}
	}
	
	public SoundManager setMusicVolume(float v) {
		MusicVolume = v;
		return this;
	}
	
	public static SoundManager setMusicVol(float v){
		Instance.MusicVolume = v;
		return Instance;
	}
	
	public AudioSource getMusicPlayer() {
		return MusicPlayer;
	}	
	
	public float getMusicVol() {
		return MusicPlayer.volume;
	}	
	
	public SoundManager setSFXVolume(float v)
	{
		SFXVolume = v;
		return this;
	}
	
	public static SoundManager setSFXVol(float v){
		Instance.SFXVolume = v;
		return Instance;
	}
	
	public AudioSource getSFXPlayer() {
		return SFXPlayer;
	}
	
	public float getSFXVol() {
		return SFXVolume;
	}

	public SoundManager stopMusic() {
		if (MusicPlayer==null) {
			MusicPlayer = this.gameObject.transform.FindChild("MusicPlayer").GetComponent<AudioSource>();
		}
		
		MusicPlayer.Stop();
		return this;
	}	
	
	public SoundManager playMusic() {
		if (MusicPlayer==null) {
			MusicPlayer = this.gameObject.transform.FindChild("MusicPlayer").GetComponent<AudioSource>();
		}
		
		if (!MusicPlayer.isPlaying && MusicPlayer.gameObject.activeSelf) {
			MusicPlayer.loop = true;
			MusicPlayer.Play();
		}
		return this;
	}

	public static void toggleAudio() {
		toggleMusic();
		toggleSoundFX();
	}

	public static void toggleMusic() {
		SoundManager.Instance.MusicEnabled = !SoundManager.Instance.MusicEnabled;
		SoundManager.setMusicVol(SoundManager.Instance.MusicEnabled ? MAX_MUSIC_VOLUME : MIN_MUSIC_VOLUME);
	}

	public static void toggleSoundFX() {
		SoundManager.Instance.SFXEnabled = !SoundManager.Instance.SFXEnabled;
		SoundManager.setSFXVol(SoundManager.Instance.SFXEnabled ? MAX_SFX_VOLUME : MIN_SFX_VOLUME);
	}

	private Dictionary<soundFX, AudioClip> preloadedSounds=new Dictionary<soundFX, AudioClip>();

	public SoundManager preloadFX( soundFX fx ){
		if( !preloadedSounds.ContainsKey(fx) ){
			preloadedSounds.Add(fx, (AudioClip) Resources.Load("SoundFX/" + fxStrings[fx],  typeof(AudioClip) ) );
		}
		return this;
	}
	public static SoundManager playDelayedFX(soundFX fx, bool exclusivePlay, float delay, bool onSecondChannel = false) {
		Instance.StartCoroutine(Instance.PlaySoundAfterDelay(fx, exclusivePlay, delay, onSecondChannel) );
		return Instance;
	}
	
	public static SoundManager playFX(soundFX fx, bool exclusivePlay = false, bool onSecondChannel = false) {
		Instance.playEffect(fx, exclusivePlay, onSecondChannel);
		return Instance;
	}
	
	private SoundManager playEffect(soundFX fx, bool playIfMute, bool onSecondChannel) {
		AudioClip clip =null;

		if( preloadedSounds.ContainsKey(fx) )
			clip = (AudioClip) preloadedSounds[fx];
		else 
			clip = (AudioClip) Resources.Load("SoundFX/" + fxStrings[fx], typeof(AudioClip));

		if(onSecondChannel) {
			SFXPlayer.PlayOneShot(clip);
		}
		else {
			if (clip != null && !(playIfMute && SFXPlayer.isPlaying)) {
				SFXPlayer.loop = false;
				SFXPlayer.clip = clip;
				SFXPlayer.Play();
			}
		}
		return this;
	}
	
	IEnumerator PlaySoundAfterDelay( soundFX fx, bool exclusivePlay, float delay, bool onSecondChannel) {
    	yield return new WaitForSeconds( delay );
		playEffect(fx, exclusivePlay, onSecondChannel);
    }
	
	private Dictionary<soundFX, string> fxStrings=new Dictionary<soundFX, string>() {
		{soundFX.batPunch, "batPunch"}
	};
}

public enum soundFX {
	batPunch
}
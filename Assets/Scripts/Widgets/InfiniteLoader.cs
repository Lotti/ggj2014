﻿using UnityEngine;
using System.Collections;

public class InfiniteLoader : FinderBaseClass {
	public bool sliced = true;
	public float vel = 150f;
	private int direction = 1;
	private float currAngle = 0;
	private Vector3 rotVector = new Vector3(0,0,1);
	private UISprite loader;

	void Awake() {
		loader = this.getSprite("loader");
	}

	void Start() {
		if (sliced) {
			loader.fillAmount=0.325f;
		}
	}
	
	void FixedUpdate() {
		if (sliced) {
			currAngle+= direction*vel*Time.deltaTime;
			loader.gameObject.transform.rotation = Quaternion.AngleAxis(currAngle,rotVector);
		}
		else {
			if (loader.invert) {
				if(loader.fillAmount == 1) {
					loader.invert = false;
					direction = -1;
				}
			}
			else {
				if (loader.fillAmount == 0) {
					loader.invert = true;
					direction = 1;
				}
			}
			loader.fillAmount+= direction*vel*Time.deltaTime;
		}
	}
}